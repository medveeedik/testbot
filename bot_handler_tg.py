import telegram

URL = "https://api.telegram.org/bot{}/"
TOKEN = '319510682:AAF-Kl41JzTGZ-XGGravlWIJysqnAVFhNP0'
CUSTOM_KEYBOARD = [['top-left', 'top-right'], ['bottom-left', 'bottom-right']]


class BotHandlerTg(object):

    def __init__(self, token=TOKEN, url=URL):

        self.token = token
        self.url = url.format(token)
        self.bot = telegram.Bot(token=self.token)

    def get_updates(self, offset=None, timeout=100):
        updates = self.bot.get_updates(offset=offset, timeout=timeout)
        return updates

    def send_message(self, chat_id, text):
        custom_keyboard = [['top-left', 'top-right'], ['bottom-left', 'bottom-right']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard)

        responce = self.bot.send_message(chat_id=chat_id, text=text,reply_markup=reply_markup)
        return responce

    def get_last_update(self):
        get_result = self.get_updates()[-1]
        return get_result