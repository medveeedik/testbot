from telegram.ext import CommandHandler, MessageHandler, Filters, Updater
from handlers import start, smartrm, saver
import sremove1
from util import get_token_from_config


def main(token=None):
    """
    Processing of messages of current bot:
    /start - greeting message
    /smartrm - rm operations
    save all text messages

    @param: token - token of current bot
    """
    current_token = token or get_token_from_config()
    updater = Updater(token=current_token)
    dispatcher = updater.dispatcher

    try:
        start_handler = CommandHandler('start', start)
        smartrm_handler = CommandHandler('smartrm', smartrm)
        msg_handler = MessageHandler(Filters.text, saver)

        dispatcher.add_handler(msg_handler)
        dispatcher.add_handler(start_handler)
        dispatcher.add_handler(smartrm_handler)
        
        updater.start_polling()

    except KeyboardInterrupt:
        exit()

if __name__ == '__main__':
    main()
