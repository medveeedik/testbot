import ConfigParser

CONF_NAME = 'bot.config'


def get_token_from_config(config_path=CONF_NAME):
    conf = ConfigParser.RawConfigParser()
    conf.read(config_path)
    return conf.get("operation", "token")
