import requests

URL = "https://api.telegram.org/bot{}/"
TOKEN = '319510682:AAF-Kl41JzTGZ-XGGravlWIJysqnAVFhNP0'


class BotHandler(object):
    """
    Main request operations
    """

    def __init__(self, token=TOKEN, url=URL):
        """
        Get whole url for next connection

        :param token: token of bot you want to interact
        :param url: template of site for using bot
        """
        self.token = token
        self.url = url.format(token)

    def get_updates(self, offset=None, timeout=100):
        """
        Get all last messages by difference  between current and previous offset/

        :param offset: offset for indicating that certain update has been seen
        :param timeout: timeout param
        :return: update request in json format
        """
        method = 'getUpdates'
        params = {'timeout': timeout, 'offset': offset}
        resp = requests.get(self.url + method, params)
        print 'r',resp
        result_json = resp.json()['result']
        print 'j',result_json
        return result_json

    def send_message(self, chat_id, text):
        """
        Reply with the message to person with this id.

        :param chat_id: id on which reply must be sent
        :param text: text of message to sent
        :return: response object of current departure
        """
        params = {'chat_id': chat_id, 'text': text}
        method = 'sendMessage'
        responce = requests.post(self.url + method, params)
        return responce

    def get_last_update(self):
        """
        Get latest update

        :return:latest update
        """
        get_result = self.get_updates()
        return get_result

