import sqlite3

DB_NAME = 'messages'


class DatabaseHandler(object):
    """
    Implementation of basic functions for storage via sqlite3
    """

    def __init__(self, db_name=DB_NAME):
        """
        Initialization of database

        @:param db_name - name of database
        """
        self.db_name = db_name
        self.connection = sqlite3.connect(self.db_name)
        self.cursor = self.connection.cursor()
        self.cursor.executescript("""CREATE TABLE IF NOT EXISTS
                                    {table_name} (msg_type text, msg text primary key, amount integer)
                                  """.format(table_name=db_name))
        self.connection.commit()

    def insert(self, msg_type, message):
        """
        Insert new record to database or increment counter for existing item

        :param msg_type: type of current msg(text/sticket/..)
        :param message: text of message
        :return: list of characteristics of currently inserted or updated item
        """
        self.cursor.executescript("""INSERT or IGNORE INTO {table_name} VALUES ("{msg_type}", "{mes}", 0);
                                     UPDATE {table_name} SET amount = amount+1 WHERE msg_type="{msg_type}" AND msg="{mes}"
                                  """.format(table_name=self.db_name, msg_type=msg_type, mes=message))
        inserted = self.cursor.execute("""SELECT * FROM  {table_name}
                                          WHERE msg_type="{msg_type}" AND msg="{mes}"
                                       """.format(table_name=self.db_name, msg_type=msg_type, mes=message))
        self.connection.commit()
        return list(*inserted)

    def close(self):
        """Close current connection to database"""
        self.connection.close()
