import re
import telegram
import sremove1
from db_handler import DatabaseHandler


def start(bot, update):
    """
    /start - greeting of users

    @param: bot - instance of bot who get message
    @param: update - request got after sending message
    """
    bot.send_message(chat_id=update.message.chat_id, text="Hi, I'm a bot!")

def smartrm(bot, update):
    """
    /smartrm - rm operations with baskets

    @param: bot - instance of bot who get message
    @param: update - request got after sending message
    """
    parser = sremove1.create_parser()
    namespace = parser.parse_args(update['message']['text'][8:].split())
    answer = sremove1.main(namespace)
    if answer:
        bot.send_message(chat_id=update.message.chat_id, text=a[0])

def saver(bot, update):
    """
    Save all text messages to db.
    """
    db  = DatabaseHandler()
    db.insert('text', update['message']['text'])
    db.close()
